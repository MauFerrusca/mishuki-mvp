var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var reload = require('browser-sync').reload;
var less = require('gulp-less');
var autoprefix = require('gulp-autoprefixer');
var wiredep = require('wiredep').stream;
var uglify = require('gulp-uglify');

/*gulp tasks*/
/*less*/
gulp.task('less', function () {
	return gulp
		.src('./build/css/*.less')
		.pipe(less())
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream());
});

gulp.task('wiredep', function () {
	gulp.src('./build/*.html')
		.pipe(wiredep({
			//      optional: 'configuration',
			//      goes: 'here'
		}))
		.pipe(gulp.dest('./dist'));
});

gulp.task('uglify', function(){
	return gulp
	.src('./build/js/app.js')
	.pipe(uglify())
	.pipe(gulp.dest('./dist/js'))
	.pipe(browserSync.stream());
});

gulp.task('sync', ['less'], function () {
	browserSync.init({
		server: {
			baseDir: "./dist",
			routes: {
				"/bower_components": "bower_components",
				"/node_modules": "node_modules"
			}
		}
	});
	
//	gulp.watch("./build/*.html").on('change', browserSync.reload);
	gulp.watch("./build/*.html", ['wiredep']).on('change', browserSync.reload);
	gulp.watch("./build/css/*.less", ['less']).on('change', browserSync.reload);
	gulp.watch("./build/js/app.js",['uglify']).on('change', browserSync.reload);
});